import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovieTest {
    Movie movie = null;
    String movieName = "Interstellar";
    int moviePrice = Movie.REGULAR;

    @BeforeEach
    public void setup() {
        movie = new Movie(movieName, moviePrice);
    }


    @Test
    void getPriceCode() {
        assertEquals(moviePrice, movie.getPriceCode());
        movie.setPriceCode(Movie.NEW_RELEASE);
        assertNotEquals(moviePrice, movie.getPriceCode());
    }

    @Test
    void setPriceCode() {
        int newPrice = Movie.CHILDRENS;
        movie.setPriceCode(newPrice);
        assertEquals(newPrice, movie.getPriceCode());
    }

    @Test
    void getTitle() {
        assertEquals(movieName, movie.getTitle());
    }
}