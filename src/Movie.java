public class Movie {
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;
    private String title;
    private Price price;
    public Movie(String newtitle, int newpriceCode) {
        title = newtitle;
        setPriceCode(newpriceCode);
    }
    public int getPriceCode() {
        return price.getPriceCode();
    }
    public void setPriceCode(int arg) {
        switch (arg) {
            case REGULAR:
                price = new RegularPrice();
                break;
            case CHILDRENS:
                price = new ChildrensPrice();
                break;
            case NEW_RELEASE:
                price = new NewReleasePrice();
                break;
            default:
                throw new IllegalArgumentException("Incorrect Price Code");
        }
    }
    public String getTitle (){
        return title;
    };

    public double getCharge(int daysRented) {
       return price.getCharge(daysRented);
    }

    public int getFrequentRenterPoints() {
       return price.getFrequentRenterPoints();
    }
}

abstract class Price {
    abstract int getPriceCode();
    int getFrequentRenterPoints() { return 1; }
    abstract double getCharge(int daysRented);
}

class ChildrensPrice extends Price{
    int getPriceCode() {return Movie.CHILDRENS; }
    double getCharge(int daysRented) {
        double result = 1.5;
        if (daysRented > 3)
            result += (daysRented - 3) * 1.5;
        return result;
    }
}

class NewReleasePrice extends Price{
    int getPriceCode() {return Movie.NEW_RELEASE;}

    @Override
    int getFrequentRenterPoints() {
       return 2;
    }

    double getCharge(int daysRented) {
        return daysRented * 3;
    }
}

class RegularPrice extends Price {
    int getPriceCode() {return Movie.REGULAR;}
    double getCharge(int daysRented) {
        double result = 2;
        if (daysRented > 2)
            result += (daysRented - 2) * 1.5;
        return result;
    }
}