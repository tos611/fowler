import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RentalTest {
    Rental rental = null;
    int daysRented = 5;
    String movieName = "Interstellar";
    int moviePrice = Movie.REGULAR;
    Movie movie = new Movie(movieName, moviePrice);

    @BeforeEach
    void setup() {
        rental = new Rental(movie, daysRented);
    }

    @Test
    void getDaysRented() {
        assertEquals(rental.getDaysRented(), daysRented);
    }

    @Test
    void getMovie() {
        assertEquals(rental.getMovie(), movie);
    }
}