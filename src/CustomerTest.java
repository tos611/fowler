import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    Customer customer = null;
    String customerName = "John";
    Rental rental = new Rental(new Movie("Interstellar", Movie.REGULAR), 5);

    @BeforeEach
    void setUp() {
        customer = new Customer(customerName);
    }

    @Test
    void getName() {
        assertEquals(customerName,  customer.getName());
    }

    @Test
    void statement() {
        Customer testCustomer = new Customer("John");
        String outputofStatement = "Rental Record for John" + "\n";
        outputofStatement += "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";
        outputofStatement += "Amount owed is " + "0.0" + "\n";
        outputofStatement += "You earned " + "0" + " frequent renter points";

        assertEquals(outputofStatement, testCustomer.statement());
    }

}